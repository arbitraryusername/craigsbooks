var myApp = angular.module('myApp',['ngRoute']);

//Configure which controller and view corresponds to each URL
//This tells Angular how to handles URLs

myApp.config(function($routeProvider, $locationProvider){
	
	$locationProvider.html5Mode(true); //to remove the /#/ from the URL
	
	$routeProvider.when('/',{
		controller:'BooksController',
		templateUrl: '/views/home.html'
	})
	.when('/books',{
		controller:'BooksController',
		templateUrl: '/views/books.html'
	})
	.when('/books/details/:id',{
		controller:'BooksController',
		templateUrl: '/views/book_details.html'
	})
	.when('/books/add',{
		controller:'BooksController',
		templateUrl: '/views/add_book.html'
	})
	.when('/books/edit/:id',{
		controller:'BooksController',
		templateUrl: '/views/edit_book.html'
	})
	.when('/books/search/:id',{
		controller:'BooksController',
		templateUrl: '/views/search_books.html'
	})
	.when('/books/about',{
		controller:'BooksController',
		templateUrl: '/views/about.html'
	})
	.when('/books/contact',{
		controller:'BooksController',
		templateUrl: '/views/contact.html'
	})
	.when('/books/legal',{
		controller:'BooksController',
		templateUrl: '/views/legal.html'
	})
	.when('/books/searchgenre/:id',{
		controller:'BooksController',
		templateUrl: '/views/search_genre.html'
	})
	.when('/books/admins',{
		controller:'BooksController',
		templateUrl: '/views/admins.html'
	})
	.when('/books/writereviews',{
		controller:'ReviewSetController',
		templateUrl: '/views/write_reviews.html'
	})
	.otherwise({
		redirectTo: '/'
	});
	
});