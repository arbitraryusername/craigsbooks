var myApp = angular.module('myApp');


myApp.controller('ReviewSetController', ['$scope', '$http', '$location', '$routeParams',
  function($scope, $http, $location, $routeParams) {

	/*	
	$scope.getReviewSets = function(){
		
		$http.get('/api/reviewsets').success(function(response){
			$scope.reviewsets = response;
		});
	}

	$scope.getReviewSet = function(){
		var id = $routeParams.id;
		
		$http.get('/api/reviewsets/'+id).success(function(response){
			$scope.reviewset = response;
			$http.put('/api/reviewset/'+id, $scope.reviewset).success(function(response){
			});
		});
	}
	
	$scope.addReviewSet = function(){
		$http.post('/api/reviewsets/', $scope.reviewset).success(function(response){
			window.location.href='/';
		});
	}

	$scope.updateReviewSet = function(){
		var id = $routeParams.id;
		$http.put('/api/reviewsets/'+id, $scope.reviewset).success(function(response){
			window.location.href='/';
		});
	}

	$scope.removeReviewSet = function(id){
		$http.delete('/api/reviewsets/'+id).success(function(response){
			window.location.href='/';
		});
	}
	*/
	
	
	/* Add logic for creating an individual review */
	
	$scope.myReview = {
		book_rating: "",
		book_title: "",
		reviewer_alias: "",
		review_body: "",
		review_ID: "new",
		is_visible: "yes"
	}
	
	$scope.review_list = []
	
	$scope.saveReview = function() {
		
		console.log("in saveReview");
		
		if($scope.myReview.review_ID == "new"){
			$scope.createReview();
		}
		else {
			$scope.updateReview($scope.myReview.review_ID);
		}
		
		// reset myReview object
		$scope.myReview.book_rating = "";
		$scope.myReview.book_title = "";
		$scope.myReview.reviewer_alias = "";
		$scope.myReview.review_body = "";
		$scope.myReview.review_ID = "new";
		$scope.myReview.is_visible = "yes";
		
		CloseModal();
	}
	
	$scope.createReview = function() {
		
		var ID = ($scope.review_list.length);
		console.log("in createReview, ID = " + ID);
		
		var newReview = {
			book_rating: $scope.myReview.book_rating,
			book_title: $scope.myReview.book_title,
			reviewer_alias: $scope.myReview.reviewer_alias,
			review_body: $scope.myReview.review_body,
			review_id: ID,
			is_visible: "yes"
		}
		$scope.review_list.push(newReview);
		
	}
		
	$scope.updateReview = function(ID){
		$scope.review_list[ID].book_rating = $scope.myReview.book_rating;
		$scope.review_list[ID].book_title = $scope.myReview.book_title;
		$scope.review_list[ID].reviewer_alias = $scope.myReview.reviewer_alias;
		$scope.review_list[ID].review_body = $scope.myReview.review_body;
	}
	
	// called when you click Edit Review button - initializes fields of modal
	$scope.editReview = function(ID) {
		console.log("in editReview, ID = " + ID);
		
		$scope.myReview.book_rating = $scope.review_list[ID].book_rating;
		$scope.myReview.book_title = $scope.review_list[ID].book_title;
		$scope.myReview.reviewer_alias = $scope.review_list[ID].reviewer_alias;
		$scope.myReview.review_body = $scope.review_list[ID].review_body;
		$scope.myReview.review_ID = ID;
		
		ReviewModal();
	}
	
	$scope.removeReview = function(ID) {
		console.log("in removeReview, ID = " + ID);
		//$scope.review_list.splice(ID, 1);
		$scope.review_list[ID].is_visible = "no";
		$scope.$apply();
	}
	
	$scope.deleteConfirmation = function(ID) {
		
		swal({   
			title: "Are you sure you want to delete this review?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#5cb85c",
			confirmButtonText: "Yes, delete!",
			closeOnConfirm: false,
			allowOutsideClick: false
			}, 
			
			function(){
				swal({
					title: "Deleted", 
					text: "This review has been deleted.", 
					type: "success",
					showConfirmButton: true,
					timer: 100
					},
					function() {
						$scope.removeReview(ID);
					}
				); 
			}
		);
	}
	
	$scope.canSubmit = function() {
		
		for(var i=0; i<$scope.review_list.length; i++){
			if($scope.review_list[i].is_visible == "yes"){
				return true;
			}
		}
		
		return false;
	}
	
  }
]);
