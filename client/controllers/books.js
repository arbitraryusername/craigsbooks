var myApp = angular.module('myApp');

myApp.service('PasswordService', function() {
	var secretPswd = 'bunny'; //fetch from database eventually
	
	this.getPassword = function(){
		return secretPswd;	
	}
	
	this.savePassword = function(newPswd) {
		secretPswd = newPswd;	
		console.log("updated secretPswd to " + secretPswd);
	}

});

myApp.controller('BooksController', ['$scope', '$http', '$location', '$routeParams', 'PasswordService',
  function($scope, $http, $location, $routeParams, PasswordService) {

	$scope.getBooks = function(){
		// ng-init="getBooks()" in books.html
		
		$http.get('/api/books').success(function(response){
			$scope.books = response;
		});
	}

	$scope.getBook = function(){
		var id = $routeParams.id;
		
		$http.get('/api/books/'+id).success(function(response){
			$scope.book = response;
			$scope.book.view_count = $scope.book.view_count + 1;
			$http.put('/api/books/'+id, $scope.book).success(function(response){
			});
		});
	}

	$scope.getSearchResults = function() {
		// ng-init="getSearchResults()" in search_books.html
		
		var queryString = $routeParams.id;
		
		$http.get('/api/books/search/' + queryString).success(function(response){
			
			var numberOfResults = response.pop(); //remove last element from array 
			
			$scope.numTitles = numberOfResults.NumTitles;
			$scope.numAuthors = numberOfResults.NumAuthors;
			
			$scope.showTitles = false;
			$scope.showAuthors = false;
			
			if(numberOfResults.NumTitles > 0) { $scope.showTitles = true };
			if(numberOfResults.NumAuthors > 0) { $scope.showAuthors = true };

			$scope.books = response;
			$scope.currentRouteParams = $routeParams.id;
			//$scope.currentURL = $location.absUrl(); //to get the whole URL
		});
	}
	
	$scope.getGenreResults = function() {
		// ng-init="getGenreResults()" in search_genre.html
		
		var queryString = $routeParams.id;
		
		$http.get('/api/books/searchgenre/' + queryString).success(function(response){
			$scope.books = response;
			$scope.currentRouteParams = $routeParams.id;
			//$scope.currentURL = $location.absUrl(); //to get the whole URL
		});
	}
	
	$scope.addBook = function(){
		$http.post('/api/books/', $scope.book).success(function(response){
			window.location.href='/books';
		});
	}

	$scope.updateBook = function(){
		var id = $routeParams.id;
		$http.put('/api/books/'+id, $scope.book).success(function(response){
			window.location.href='/books';
		});
	}

	$scope.removeBook = function(id){
		$http.delete('/api/books/'+id).success(function(response){
			window.location.href='/books';
		});
	}
	
	$scope.deleteBookConfirmation = function(id) {
		var book_id = id;
		
		swal({   
			title: "Are you sure you want to delete this book?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#5cb85c",
			confirmButtonText: "Yes, delete!",
			closeOnConfirm: false,
			allowOutsideClick: true
			}, 
			
			function(){
				swal({
					title: "Deleted", 
					text: "This book has been deleted.", 
					type: "success",
					timer: 1500
					},
					function() {
						$scope.removeBook(book_id);
					}
				); 
			}
		);
	}
	
	$scope.adminLogin = function() {
		$scope.currentPswd = PasswordService.getPassword();

		swal({   
			title: "Login as Admin",
			type: "input",   
			showCancelButton: true,
			confirmButtonColor: "#5cb85c",
			closeOnConfirm: false,
			animation: "slide-from-top",
			allowOutsideClick: true,
			inputPlaceholder: "enter your admin password",
			inputType: "password"
			},
			function(inputValue){
				if (inputValue === false) {
					return false;
				}
				if ( !($scope.IsPswdCorrect(inputValue)) ){	
					swal.showInputError("Incorrect Password!");
					return false;
				}
				else {
					swal({
						title: "Login Successful!", 
						text: "Welcome back, Admin!", 
						type: "success",
						showConfirmButton: false,
						timer: 1250
						},
						function(){
							$scope.toggleIsAdmin;
							window.location.href='/books/admins';
						}
					); 
				}
			}
		);
	}
	
	$scope.UpdateAdminPswd = function(newPswd) {
		PasswordService.savePassword(newPswd);
		$scope.Pswd = null;
		$scope.newPswd = null;
		$scope.confirmPswd = null;
		
		swal({
			title: "Admin Password Updated!",
			type: "success",
			showConfirmButton: false,
			timer: 1250
			}
		);
	}

	$scope.IsPswdCorrect = function(enteredPswd) {
		var actualPswd = PasswordService.getPassword();
		
		if(actualPswd == enteredPswd){
			return true;
		}
		else{
			return false;		
		}
	}

	$scope.IsNewPswdValid = function(newPswd) {
		if(newPswd != null && newPswd.length > 4) {
			return true;
		}
		else {
			return false;
		}
	}
	
	$scope.IsNewPswdConfirmed = function(pswd1, pswd2) {
		if($scope.IsNewPswdValid(pswd1) && pswd1 == pswd2){
			return true;
		}
		else {
			return false;
		}
	}

  }
]);
