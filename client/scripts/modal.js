function ReviewModal() {
  document.getElementById("body").className = "dialogIsOpen";

  $("#modal").dialog({
    modal: true,
    title: 'Compose a Book Review',
    draggable: true,
    resizable: true,
    closeOnEscape: true,
    width: 750,
    beforeClose: function() {
      document.getElementById("body").className = "";
    },
    buttons: {
      "Save Review": function () {
        $(this).dialog("close");
        //document.getElementById("body").className = "";
      }
    }
  });
}

function CloseModal() {
  $("#modal").dialog("close");
}

