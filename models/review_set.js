var mongoose = require('mongoose');

// ReviewSet Schema
var reviewSetSchema = mongoose.Schema({
	create_date:{
		type: Date,
		default: Date.now
	},
	reviewer_email:{
		type: String,
	},
	review_list:{
		type: [Review],
		required: true
	},
  reviewer_name:{
		type: String,
		required: true
	}                                
});

var ReviewSet = module.exports = mongoose.model('ReviewSet', reviewSchema);

// Get All ReviewSets
module.exports.getReviewSets = function(callback, limit){
	//you can pass in an integer for limit, which limits number of returned results
	ReviewSet.find(callback).limit(limit);
}

// Get ReviewSet by id
module.exports.getReviewSetById = function(id, callback){
	ReviewSet.findById(id, callback);
}

// Search For ReviewSets by a particular person
module.exports.getReviewsByReviewer = function(id, callback){
	var queryString = id;
	ReviewSet.find( {"reviewer_name" : { "$regex": queryString, "$options": "i"} }, callback);
}

// Add a ReviewSet (create in database)
module.exports.addReviewSet = function(reviewset, callback){
	ReviewSet.create(reviewset, callback);
}

// Update ReviewSet
module.exports.updateReviewSet = function(id, reviewset, options, callback){
	var query = {_id: id};
	var updatedReviewSet = {
		reviewer_email: reviewset.reviewer_email,
		reviewer_name: reviewset.reviewer_name,
		review_list: review.review_list
	}
	
	ReviewSet.findOneAndUpdate(query, updatedReviewSet, options, callback);
}

// Delete ReviewSet
module.exports.removeReviewSet = function(id, callback){
	var query = {_id: id};
	ReviewSet.remove(query, callback);
}
