var mongoose = require('mongoose');

// Review Schema
var reviewSchema = mongoose.Schema({
	book_title:{
		type: String,
		required: true
	},
	book_rating:{
		type: String,
		required: true
	},
	create_date:{
		type: Date,
		default: Date.now
	},
	review_body:{
		type: String,
    required: true
	},
	review_likes:{
		type: String,
		default: 0
	},
	reviewer_alias:{
		type: String,
		default: "anonymous"
	},
});

var Review = module.exports = mongoose.model('Review', reviewSchema);

// Get All Reviews
module.exports.getReviews = function(callback, limit){
	//you can pass in an integer for limit, which limits number of returned results
	Review.find(callback).limit(limit);
}

// Get Review by id
module.exports.getReviewById = function(id, callback){
	Review.findById(id, callback);
}

// Search For Reviews with a particular book name
module.exports.getReviewsByBookId = function(id, callback){
	var queryString = id;
	Review.find( {"book_name" : { "$regex": queryString, "$options": "i"} }, callback);
}

// Add a Review
module.exports.addReview = function(review, callback){
	Review.create(review, callback);
}

// Update Review
module.exports.updateReview = function(id, review, options, callback){
	var query = {_id: id};
	var update = {
		book_name: review.book_name,
		rating: review.rating,
		review_body: review.review_body,
		review_likes: review.review_likes,
		reviewer_alias: review.reviewer_alias
	}
	
	Review.findOneAndUpdate(query, update, options, callback);
}

// Delete Review
module.exports.removeReview = function(id, callback){
	var query = {_id: id};
	Review.remove(query, callback);
}