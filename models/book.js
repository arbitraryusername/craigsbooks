var mongoose = require('mongoose');

// Book Schema
var bookSchema = mongoose.Schema({
	title:{
		type: String,
		required: true
	},
	genre:{
		type: String,
		required: true
	},
	description:{
		type: String
	},
	author:{
		type: String,
		required: true
	},
	publisher:{
		type: String
	},
	pages:{
		type: String
	},
	image_url:{
		type: String,
		default: "http://cdn2.hubspot.net/hub/200670/file-1250303394-jpg/images/question-mark-scratch-head-783x1030.jpg"
	},
	buy_url:{
		type: String,
		default: "http://www.barnesandnoble.com/b/books/"
	},
	create_date:{
		type: Date,
		default: Date.now
	},
	view_count:{
		type: Number,
		default: 0
	}
});

var Book = module.exports = mongoose.model('Book', bookSchema);

// Get Books
module.exports.getBooks = function(callback, limit){
	//you can pass in an integer for limit, which limits number of returned results
	Book.find(callback).limit(limit);
}

// Get Book by id
module.exports.getBookById = function(id, callback){
	Book.findById(id, callback);
}

// Search Book Names by user's query
module.exports.getBooksByTitle = function(id, callback){
	var queryString = id;
	Book.find( {"title" : { "$regex": queryString, "$options": "i"} }, callback);
}

module.exports.getBooksByAuthor = function(id, callback){
	var queryString = id;
	Book.find( {"author" : { "$regex": queryString, "$options": "i"} }, callback);
}

module.exports.getBooksByGenre = function(id, callback){
	var queryString = id;
	Book.find( {"genre" : { "$regex": queryString, "$options": "i"} }, callback);
}

// Add Book
module.exports.addBook = function(book, callback){
	Book.create(book, callback);
}

// Update Book
module.exports.updateBook = function(id, book, options, callback){
	var query = {_id: id};
	var update = {
		title: book.title,
		genre: book.genre,
		description: book.description,
		author: book.author,
		publisher: book.publisher,
		pages: book.pages,
		image_url: book.image_url,
		buy_url: book.buy_url,
		view_count: book.view_count
	}
	
	Book.findOneAndUpdate(query, update, options, callback);
}

// Delete Book
module.exports.removeBook = function(id, callback){
	var query = {_id: id};
	Book.remove(query, callback);
}
