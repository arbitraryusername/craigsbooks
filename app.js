var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

app.use(express.static(__dirname + '/client'));
app.use(bodyParser.json());

Genre = require('./models/genre');
Book = require('./models/book');
Review = require('./models/review');

// Connect to Mongoose
mongoose.connect('mongodb://localhost/bookstore'); //our URI (uniform resource identifier)
var db = mongoose.connection;


// send the index.html for requests to support HTML5Mode
app.use('/books*', function(req, res, next) {
  
  res.sendFile('/client/index.html', { root: __dirname + '/'});
  
  // the root is the directory that THIS file sits in
});


// development environment error handler - print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
  });
}

// production environment error handler - no stacktraces shown to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
});


app.get('/', function(req, res){
	res.send('Please use /api/books or /api/genres');
});

/*
app.get('/api/genres', function(req, res){
	Genre.getGenres(function(err, genres){
		if(err){
			throw err;
		}
		res.json(genres);
	});
});

app.post('/api/genres', function(req, res){
	var genre = req.body;
	Genre.addGenre(genre, function(err, genre){
		if(err){
			throw err;
		}
		res.json(genre);
	});
});

app.put('/api/genres/:_id', function(req, res){
	var id = req.params._id;
	var genre = req.body;
	Genre.updateGenre(id, genre, {}, function(err, genre){
		if(err){
			throw err;
		}
		res.json(genre);
	});
});

app.delete('/api/genres/:_id', function(req, res){
	var id = req.params._id;
	Genre.removeGenre(id, function(err, genre){
		if(err){
			throw err;
		}
		res.json(genre);
	});
});
*/

app.get('/api/books', function(req, res){
    Book.getBooks(function(err, books){
			if(err){
				throw err;
			}
			res.json(books);
	});
});

app.get('/api/books/:_id', function(req, res){
	Book.getBookById(req.params._id, function(err, book){
		if(err){
			throw err;
		}
		res.json(book);
	});
});

app.get('/api/books/search/:_id', function(req, res){

  var booksResult;
	var authorsResult;
	var resultsArray = []; //create new JavaScript array
	
	function numResults(numTitles, numAuthors) {
		this.NumTitles = numTitles;
		this.NumAuthors = numAuthors;
	}
	
	Book.getBooksByTitle(req.params._id, function(err, books) {
		if(err){
			throw err;
		}
		booksResult = books;
		
		Book.getBooksByAuthor(req.params._id, function(err, authors) {
			if(err) {
				throw err;
			}
			
			authorsResult = authors;
			
			if(booksResult.length > 0) {
				for(var i = 0; i< booksResult.length; i++) {
					resultsArray.push(booksResult[i]);
				}
			}
			if(authorsResult.length > 0 ){
				for(var i = 0; i < authorsResult.length; i++) {
					resultsArray.push(authorsResult[i]);
				}
			}
			
			var numberOfResults = new numResults(booksResult.length, authorsResult.length);
			
			resultsArray.push(numberOfResults);
			
			res.json(resultsArray);
		});
	});
});

app.get('/api/books/searchgenre/:_id', function(req, res){
  Book.getBooksByGenre(req.params._id, function(err, books){
    if(err) {
      throw err;
    }
    res.json(books)
  });
});

app.post('/api/books', function(req, res){
	var book = req.body;
	Book.addBook(book, function(err, book){
		if(err){
			throw err;
		}
		res.json(book);
	});
});

app.put('/api/books/:_id', function(req, res){
  var id = req.params._id;
	var book = req.body;
  
	Book.updateBook(id, book, {}, function(err, book){
		if(err){
			throw err;
		}
		res.json(book);
	});
});

app.delete('/api/books/:_id', function(req, res){
	var id = req.params._id;
	Book.removeBook(id, function(err, book){
		if(err){
			throw err;
		}
		res.json(book);
	});
});

app.listen(3000);
console.log('Running on port 3000...');