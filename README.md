## CraigsBooks Overview ##

**CraigsBooks** is a modern web app written on the MEAN stack (**M**ongoDB, **E**xpressJS, **A**ngularJS, and **N**odeJS). Every web page is carefully designed to look great on any screen size.

You can add books (with a title, author, publisher, summary, genre, cover image), edit existing books, and delete books. You can search all books in the database by title, author, and genre.

The home page of this app features an animated carousel with images of books, book reviews, the 6 most recently added books, and the 6 most viewed books

Some fun things used in this project: Bootstrap, Sweet Alert (http://t4t5.github.io/sweetalert/), Google Fonts

## How to Run the Application Yourself ##
Clone or download the repo. Launch the NPM terminal and navigate to the root directory of the project. Run "npm install" to install all dependencies. Run "node app" to start the application. Then open your browser and navigate to [http://localhost:3000](Link URL)

In order for you to save books or load the provided database (located at /database/books.bson) , you will need to launch an instance of MongoDB in a separate terminal window.

## Please Note ##
This is still a work in progress. Some features, such as the Admin Console and the Book Reviews, are not fully implemented.


## Screenshots ##
![craigsbooks_home.PNG](https://bitbucket.org/repo/adBXr9/images/2052923392-craigsbooks_home.PNG)

![craigsbooks_searchresultsforharry.PNG](https://bitbucket.org/repo/adBXr9/images/3597206864-craigsbooks_searchresultsforharry.PNG)

![craigsbooks_bookdetailspage.PNG](https://bitbucket.org/repo/adBXr9/images/2372996734-craigsbooks_bookdetailspage.PNG)

![craigsbooks_addbookpage.PNG](https://bitbucket.org/repo/adBXr9/images/3726714587-craigsbooks_addbookpage.PNG)